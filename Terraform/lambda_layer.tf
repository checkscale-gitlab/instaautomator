resource "aws_lambda_layer_version" "automator-lambda-layer" {
  layer_name = "automator-lambda-layer"
  filename = "../bin/lambda_layer.zip"
  source_code_hash = filebase64sha256("../bin/lambda_layer.zip")
  compatible_runtimes = ["python3.7"]
}