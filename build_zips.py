import shutil
import logging

log = logging.getLogger("Logger")

shutil.make_archive(base_name='bin/lambda_automator', format='zip', base_dir='lambda_automator', logger=log)
shutil.make_archive(base_name='bin/lambda_layer', format='zip', base_dir='lambda_layer', logger=log)
