#!/usr/bin/env python

from setuptools import setup, find_packages
import sys

version = 0
name = None

if '--version' in sys.argv:
    index = sys.argv.index('--version')
    sys.argv.pop(index)
    version = sys.argv.pop(index)
if '--name' in sys.argv:
    index = sys.argv.index('--name')
    sys.argv.pop(index)
    version = sys.argv.pop(index)

setup(
    name=name,
    version=version,
    description="Python automator code",
    include_package_data=True,
    packages=find_packages(".", exclude=["Code"])
)
